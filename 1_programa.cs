using System;


namespace ejercici1
{
    public class Persona
    {
        protected string nombre;
        protected string apellido;
        protected double cedula;
        protected int edad;
        

        public string Nombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }
        
        public string Apellido
        {
            set
            {
                apellido = value;
            }
            get
            {
                return apellido;
            }
        }
        
        public double  Cedula 
        {
            set
            {
                cedula = value;
            }
            get
            {
                return cedula; 
            }
        }

        public int Edad
        {
            set
            {
                edad = value;
            }
            get
            {
                return edad;
            }
        }

        public void Imprimir()
        {
            Console.WriteLine("Nombre: "   + Nombre);
            Console.WriteLine("Apellido: "  + Apellido);
            Console.WriteLine("Cedula: "   + Cedula);
            Console.WriteLine("Edad: "  + Edad);
        } 
    }

    public class Profesor : Persona
    {
        protected float sueldo;

        public float Sueldo
        {
            set
            {
                sueldo = value;
            }
            get
            {
                return sueldo;
            }
        }

        new public void Imprimir()
        {
            base.Imprimir();
            Console.WriteLine("Sueldo: " + Sueldo);
        }
    }

    class ejercicio1
    {
        static void Main(string[] args)
        {
            Persona cjpersona = new Persona();
            cjpersona.Nombre = "Rafael";
            cjpersona.Edad = 19;
            cjpersona.Apellido = "Casado";
            cjpersona.Cedula = 4023829326;
            Console.WriteLine("DATOS DE LA PERSONA:  ");
            cjpersona.Imprimir();
            

            Profesor cjprofesor = new Profesor();
            cjprofesor.Nombre = "Junior";
            cjprofesor.Edad= 25;
            cjprofesor.Apellido = "Casado";
            cjprofesor.Cedula = 40207643667;
            cjprofesor.Sueldo = 40000;
            Console.WriteLine("DATOS DEL TEACHER:");
            cjprofesor.Imprimir();

            Console.ReadKey();
        }
    }
}